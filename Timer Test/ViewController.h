//
//  ViewController.h
//  Timer Test
//
//  Created by Martin Kelly on 29/07/2015.
//  Copyright (c) 2015 Martin Kelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIPickerView *timePicker;
@property (nonatomic, strong) NSArray *pickerData;
@property (weak, nonatomic) IBOutlet UIButton *next;

@end

