//
//  SecondViewController.h
//  Timer Test
//
//  Created by Martin Kelly on 29/07/2015.
//  Copyright (c) 2015 Martin Kelly. All rights reserved.
//

#import "ViewController.h"

@interface SecondViewController : ViewController

@property (nonatomic) int seconds;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end
