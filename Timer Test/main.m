//
//  main.m
//  Timer Test
//
//  Created by Martin Kelly on 29/07/2015.
//  Copyright (c) 2015 Martin Kelly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
