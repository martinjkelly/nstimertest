//
//  SecondViewController.m
//  Timer Test
//
//  Created by Martin Kelly on 29/07/2015.
//  Copyright (c) 2015 Martin Kelly. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [NSTimer scheduledTimerWithTimeInterval:7 target:self selector:@selector(timeout)
                                   userInfo:nil repeats:NO];
    
    [NSTimer scheduledTimerWithTimeInterval:self.seconds target:self selector:@selector(setTimeToLabel) userInfo:nil repeats:NO];
}

-(void)timeout
{
    // pops to root view
}


- (void)setTimeToLabel
{
    self.seconds = self.seconds - 1;
    self.timeLabel.text = [NSString stringWithFormat:@"%d", self.seconds];
}

@end
