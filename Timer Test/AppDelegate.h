//
//  AppDelegate.h
//  Timer Test
//
//  Created by Martin Kelly on 29/07/2015.
//  Copyright (c) 2015 Martin Kelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

